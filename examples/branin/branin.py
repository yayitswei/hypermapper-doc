import math, sys
import requests
import json

PROD_API_DOMAIN = "https://api.satoshi.app"

def branin_function(x1, x2):
    """
    Compute the branin function.
    :param x1: the first input of branin.
    :param x2: the second input of branin.
    :return: the value of the braning function and the (fake) energy used to compute that function.
    """
    a = 1.0
    b = 5.1 / (4.0 * math.pi * math.pi)
    c = 5.0 / math.pi
    r = 6.0
    s = 10.0
    t = 1.0 / (8.0 * math.pi)

    y_value = a * (x2 - b * x1 * x1 + c * x1 - r) ** 2 + s * (1 - t) * math.cos(x1) + s

    return y_value

def get_result(request):
    x1 = float(request['x1'])
    x2 = float(request['x2'])
    y_value = branin_function(x1, x2)
    return {'x1': x1, 'x2': x2, 'Value': y_value, '_timestamp': 0}

def get_results(hm_requests):
    return list(map(get_result, hm_requests))

def run_experiment(token, api_domain):
    print("Using endpoint: ", api_domain)

    with open('./config.json') as json_file:
        config = json.load(json_file)

    print("Using config:")
    print(config)

    r = requests.post(api_domain + '/experiment?token=' + token, json=config).json()

    print("Setting up new experiment. Sending to server:")
    print(config)
    print("Got back:")
    print(r)
    print("\n")

    experiment_id = r['experiment_id']
    hm_requests = r['requests']

    print('New experiment initialized. experiment_id: ', experiment_id)
    print("\n")

    for i in range(config["optimization_iterations"]):
        print("Iteration %d" %i)
        results = get_results(hm_requests)
        iteration = {'results': results, 'iteration_number': i}
        print('Sending to server:', iteration)
        r = requests.post(api_domain + '/experiment/' + experiment_id + '/iteration?token=' + token, json=iteration).json()
        hm_requests = r['requests']
        print('Got back:', r)
        print("\n")

def main():
    if len(sys.argv) > 2:
        domain = sys.argv[2]
    else:
        domain = PROD_API_DOMAIN

    run_experiment(sys.argv[1], domain)
    print("End of Branin.")


if __name__ == "__main__":
    main()
