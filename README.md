# Quick Start Guide

HyperMapper is a black-box optimizer.

You are looking for a parameter setting that minimizes some performance metric of your algorithm (the black-box function), such as runtime, error, or cost. 
To use HyperMapper for this purpose you need to tell it about your parameters and how to evaluate the performance of the black-box. 
Here, we will show how to do this on a running example using a simple black-box function called the [Branin](https://www.sfu.ca/~ssurjano/branin.html) function. 
We look for minimizing the value of this function given the two parameters x1 and x2.

# The Branin Black-box Function
Consider the [Branin](https://www.sfu.ca/~ssurjano/branin.html) black-box function evaluation (which depends on the input variables x1 and x2), 
this is the objective we want to minimize: 
```python
def branin_function(X):
    x1 = X['x1']
    x2 = X['x2']
    a = 1.0
    b = 5.1 / (4.0 * math.pi * math.pi)
    c = 5.0 / math.pi
    r = 6.0
    s = 10.0
    t = 1.0 / (8.0 * math.pi)
    y_value = a * (x2 - b * x1 * x1 + c * x1 - r) ** 2 + s * (1 - t) * math.cos(x1) + s
    return y_value
```

An example of this code can be found in [branin.py](https://gitlab.com/yayitswei/hypermapper-doc/blob/master/examples/branin/branin.py). 

### Setup HyperMapper to Run on Branin 

To use HyperMapper SaaS, first retrieve your API token from the web app and then use the API to create an experiment and submit results.

If you want to access the stand-alone version of HyperMapper contact luiginardi@gmail.com.

# Web App

The web application can be found at https://app.hypermapper.com.

Here you can:
- signup and login to use the software
- retrieve the API token
- view the results of ongoing and past experiments

# API

To start an experiment and send your results to the optimizer you will use the API. 
In order to use the API you will first need to signup and obtain an API token at https://app.hypermapper.com/api-token


## URL

The API is accessible at https://api.hypermapper.com

## Endpoints

### Create Experiment

An experiment is intended as an optimization run where the client (the black-box) and the app interact and exchange information.

The experiment is initiated by the client with a POST request that submits the experiment configuration to the app.


`POST /experiment`

In: config json

Out: evaluations to run, experiment id (UUID)

#### Config json

The Json config needs to contain:
- `experiment_name`: User-defined name for the experiments. Example:
```
"experiment_name": "branin"
```
- `optimization_objectives`: List that defines the names and the number of the objectives to optimize. 
HyperMapper will automatically infer if this application is a mono or multi-objective optimization problem. 
Mono-objective example:
```
"optimization_objectives": ["Runtime"]
```
Two-objective example:
```
"optimization_objectives": ["Value", "Energy"]
```
Four-objective example:
```
"optimization_objectives": ["Value", "Energy", "Runtime", "Latency"]
```
- `input_parameters`: Object containing the names, types, values and defaults for each of the parameters in the search space.
  - `parameter name`: The name of the parameter being declared.
  - `parameter_type`: The parameter type, one of `"real"`, `"integer"`, `"ordinal"` or `"categorical"`.
  - `values`: The range of admissible values for the parameter.
  - `parameter_default`: The defauls value for the parameter (optional field).
Example:
```
"input_parameters" : {
      "x1": {
          "parameter_type" : "real",
          "values" : [-5, 10],
          "parameter_default" : 0
      }
```
- `optimization_iterations`: Usually a number between 1 and 10. Max number of optimization iterations in the current experiment. Usually, a bigger number will give a better accuracy but slower results. Default is 5.

Additional optional parameters are explained [here](https://github.com/luinardi/hypermapper/wiki/HyperMapper-Json-Parameters-File).


Here is an example of how to initiate an experiment named "branin" that seeks to minimize a mono-objective function with 2 real-valued input parameters `x1` and `x2` over a total of 50 iterations. 
Replace ```<YOUR-API-TOKEN>``` with your token in https://app.hypermapper.com/api-token.


#### Example Request
Run the following from a shell: 
```
curl -X "POST" "https://api.hypermapper.com/experiment?token=<YOUR-API-TOKEN>"  \
  -H 'Content-Type: application/json; charset=utf-8' \
  -d $'{
        "experiment_name": "branin",
        "optimization_iterations": 50,
        "optimization_objectives": [
          "Value"
        ],
        "input_parameters": {
          "x1": {
            "parameter_type": "real",
            "values": [
              -5,
              10
            ],
            "parameter_default": 0
          },
          "x2": {
            "parameter_type": "real",
            "values": [
              0,
              15
            ],
            "parameter_default": 0
          }
        }
      }'
```

This call returns:
- `experiment_id`: the UUID of the experiment
- `requests`: a list of initial evaluations to run in the black-box function
- `iteration_number`: the iteration number

Example of returned values:
```
{
   "experiment_id":"f009a9ee-c5f1-4a1e-ae30-1514eb54491b",
   "requests":[
      {
         "x1":2.804150103660689,
         "x2":7.148745304195122
      },
      {
         "x1":-0.06433716274170287,
         "x2":10.679652149877878
      },
      {
         "x1":-4.650833934760474,
         "x2":11.317163546988862
      }
   ],
   "iteration_number":0
}
```


### Submit Test Data To an Experiment

Once an experiment has been created, the client will dialogue with the app by submitting the results of the requested evaluations through a post request at the end point:

`POST /experiment/:id/iteration`

In: experiment id, iteration number, results of the evaluations, timestamp/evaluationId

Out: Next evaluations to run


#### Example Request
Run the following from a shell: 
```
curl -X "POST" "https://api.hypermapper.com/experiment/133e1c7b-07a0-49bf-b50e-a90790437920/iteration?token=<YOUR-API-TOKEN>"  -H 'Content-Type: application/json; charset=utf-8'      -d $'{
   "results":[
      {
         "x1":-2.0075866655683905,
         "x2":2.6946717115059697,
         "Value":55.23477930074141,
         "_timestamp":0
      },
      {
         "x1":7.63734143082298,
         "x2":2.794517233690267,
         "Value":14.064843866738148,
         "_timestamp":1
      },
      {
         "x1":-3.5414616751490584,
         "x2":7.450699476464113,
         "Value":34.86429025378014,
         "_timestamp":2
      }
   ],
   "iteration_number":0
}'
```


### Get Optimization Results

The results of an experiment can be queried at any time in the optimization loop with a get request.

`GET /experiment/:experiment_id`

In: experiment id

Out: results stored in the db so far

#### Example Request
```
curl -X "GET" "https://api.hypermapper.com/experiment/133e1c7b-07a0-49bf-b50e-a90790437920?token=<YOUR-API-TOKEN>"
```
They are also accessible via the web app at: https://app.hypermapper.com/experiments once logged in.

## Putting It All Together
This is a complete example in Python that runs an optimization of the Branin black-box function. 
Replace ```<YOUR-API-TOKEN>``` with your token in https://app.hypermapper.com/api-token.

```bash
git clone https://gitlab.com/yayitswei/hypermapper-doc.git
cd hypermapper-doc/examples/branin
python3 branin.py <YOUR-API-TOKEN>
```
